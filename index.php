<?php

/**
 *  Ensure that the requested type (if specified) is only one of the acceptable
 *  types from below, and exit immediately if it is not before executing any
 *  other code.
 */
$acceptableFormats = [
    'text/html',
    'application/json',
    'text/plain',
];

$format = $_REQUEST['format'] ?? 'text/html';

if (! in_array($format, $acceptableFormats)) {
    die('Invalid format, "' . htmlentities($format) . '" specified');
}

if (! isset($_SERVER['REMOTE_ADDR'])) {
    die('Could not detect REMOTE_ADDR');
}

$remoteIPAddress = $_SERVER['REMOTE_ADDR'];
$remoteHostName = gethostbyaddr($remoteIPAddress);

if (false === $remoteHostName) {
    die('Invalid IP address input');
}

if ($remoteHostName === $remoteIPAddress) {
    //die('Hostname lookup for IP address "' . $remoteIPAddress . '" failed.');
    $remoteHostName = 'Remote hostname lookup failed';
}

$data = [
    'remoteIPAddress' => $remoteIPAddress,
    'remoteHostName' => $remoteHostName,
];

header('Content-Type: ' . $format);

switch ($format) {
    case 'application/json':
        require_once __DIR__ . '/index_json.inc';
        break;
    case 'text/plain':
        require_once __DIR__ . '/index_text.inc';
        break;
    default:
        //break;
    case 'text/html':
        require_once __DIR__ . '/index_html.inc';
        break;
}

//  Make sure there's no more execution after this point.
die();
