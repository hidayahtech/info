<?php

/**
 * This variable is defined in a script that includes this one
 *
 * @var array $data
 */

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>IP Address Info</title>
    <style>
        .centered {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        table, tr, th, td {
            border: solid black 1px;
            font-size: xx-large;
            text-align: center;
        }
        th {
            background: slategray;
            color: snow;
        }
    </style>
  </head>
  <body>
    <div class="centered">
      <table>
        <thead>
          <tr>
            <th>Remote IP Address</th>
            <th>Remote Hostname</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?=htmlentities($data['remoteIPAddress'])?></td>
            <td><?=htmlentities($data['remoteHostName'])?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="centered">
      <div class="inner-container">
        <p>View this data in additional formats:</p>
        <ul>
          <li><a href="?format=text/plain">Plain Text</a></li>
          <li><a href="?format=application/json">JSON</a></li>
        </ul>
      </div>
    </div>
  </body>
</html>
