<?php

/**
 * This variable is defined in a script that includes this one
 *
 * @var array $data
 */

foreach ($data as $key => $value) {
    echo "$key: $value" . PHP_EOL;
}

