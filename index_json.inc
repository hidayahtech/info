<?php

/**
 * This variable is defined in a script that includes this one
 *
 * @var array $data
 */

echo json_encode($data);
